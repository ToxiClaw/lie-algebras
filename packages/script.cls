\NeedsTeXFormat{LaTeX2e}

% Use KOMA book class
\ProvidesClass{script}[11.08.2019 Version 1.1]
%\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrartcl}}
%\ProcessOptions\relax
\RequirePackage{pgfopts}

\def\extra@options{}
\pgfkeys{/script/.is family, /script,
	fontsize/.initial=11pt,
	DIV/.initial=calc,
	twoside/.initial=false,
	.unknown/.code={\edef\extra@options{\extra@options,\pgfkeyscurrentname}}
}
\newcommand\DCO[1]{#1=\pgfkeysvalueof{/script/#1}}
\newcommand\CO[1]{\pgfkeysvalueof{/script/#1}}

\ProcessPgfOptions{/script}
\LoadClass[
	\extra@options,
	\DCO{fontsize},
	\DCO{DIV},
	\DCO{twoside},
]{scrbook}
%\LoadClass[11pt, DIV=calc, twoside=false]{scrbook}

% Extern Includes
\RequirePackage{mathtools}
\RequirePackage{amsmath, amssymb}
\RequirePackage[amsmath, thmmarks, framed]{ntheorem}

% Functionality Includes
\RequirePackage{scrlayer-scrpage, setspace}
\RequirePackage{ifxetex, ifluatex}
\RequirePackage{enumitem, booktabs, afterpage, totcount, titling}
\RequirePackage[framemethod=tikz]{mdframed}
\RequirePackage[
	unicode,
	colorlinks=false,
	pdfborder={0 0 0},
	bookmarks,
	bookmarksopen,
	bookmarksnumbered,
	bookmarksdepth]{hyperref}

% Class Macros
\newif\ifxetexorluatex
\ifxetex
	\xetexorluatextrue
\else \ifluatex
	\xetexorluatextrue
\else
	\xetexorluatexfalse
\fi \fi

% Configuration
\ifxetexorluatex
	% Specific Includes
	\RequirePackage[babelshorthands]{polyglossia}
	\setdefaultlanguage{german}
	\PolyglossiaSetup{german}{indentfirst=true}
	
	\RequirePackage[
		backend=biber,
		style=alphabetic,
		abbreviate=false]{biblatex}
	
	\RequirePackage[german=quotes]{csquotes}
	\RequirePackage{fontspec}
	\RequirePackage[capitalise, nameinlink]{cleveref}
	\RequirePackage[warnings-off={mathtools-colon,mathtools-overbracket}]{unicode-math}

	%% Fonts
	\setmainfont[BoldFont=Libertinus Serif SemiBold]{Libertinus Serif}
	\setmathfont{Libertinus Math}
	\setmathfont[range={\mathbb}]{XITS Math}
	\setmathfont[range={\mathcal}]{Latin Modern Math}
	\setmathfont[range={\mathfrak}]{Latin Modern Math}
\else
	\ClassError{paper}{Not supported compiler.} {Please use LuaTeX or XeTeX.}
\fi

%% Getter/Setter
\def\thetitle{}
\let\oldTitle\title
\renewcommand*{\title}[1]{\oldTitle{#1}\def\thetitle{#1}}

\def\thesubject{}
\let\oldSubject\subject
\renewcommand*{\subject}[1]{\oldSubject{#1}\def\thesubject{#1}}

\def\theauthor{}
\let\oldAuthor\author
\renewcommand*{\author}[1]{\oldAuthor{#1}\def\theauthor{#1}}

\def\thedate{}
\let\oldDate\date
\renewcommand*{\date}[1]{\oldDate{#1}\def\thedate{#1}}

\def\thekeywords{}
\newcommand*{\keywords}[1]{\def\thekeywords{#1}}

\hypersetup{
	pdftitle={\thetitle},
	pdfauthor={\theauthor},
	pdfsubject={\thesubject},
	pdfkeywords={\thekeywords}
}

% Header and Footer
\clearscrheadfoot
\ohead{\normalfont \headmark}
\cfoot[\pagemark]{\pagemark}
\automark{section}
\pagestyle{scrheadings}

%% Layout
\setkomafont{chapter}{\LARGE\centering}
\setkomafont{section}{\Large}
\setkomafont{disposition}{\bfseries\rmfamily\boldmath}

%\setstretch{1.1}
%\setlength\droptitle{-3cm}
\setlength\parskip{0pt}
\setlength\parindent{1em}

\pretitle{\begin{center}\LARGE\textbf}
\posttitle{\par\end{center}}
\renewcommand{\maketitlehookb}{\begin{center}\large\thesubject\par\end{center}\vskip 0.5em}
\preauthor{\begin{center}\Large}
\postauthor{\par\end{center}}
\predate{\begin{center}\Large}
\postdate{\par\end{center}}

% Counters & Environments
%% Enumeration Style
\setenumerate{label=(\alph*),leftmargin=2em}
\newlist{wenumerate}{enumerate}{1}
\setlist[wenumerate]{leftmargin=3em}
\setlistdepth{9}

%% Theorem Macro
\DeclareDocumentCommand\newmdtheoremenv{s O{} m o m o }
{
	\IfBooleanTF{#1} {
	\newtheorem*{#3}{#5}
	}
	{
	\ifboolexpr{ test {\IfNoValueTF {#4}} and test {\IfNoValueTF {#6}} }
	{
		\newtheorem{#3}{#5}
	}
	{
		\IfValueTF{#4}{\newtheorem{#3}[#4]{#5}}{}%
		\IfValueTF{#6}{\newtheorem{#3}{#5}[#6]}%
	}
	}
	\BeforeBeginEnvironment{#3}{\begin{mdframed}[#2]}
	\AfterEndEnvironment{#3}{\end{mdframed}}
}

% Colors
%\definecolor{bluegray}{rgb}{0.7, 0.85, 0.85}
\definecolor{bluegray}{rgb}{0.5, 0.65, 0.85}
\definecolor{redgray}{rgb}{0.85, 0.4, 0.4}

%% Theorem Style
\mdfdefinestyle{thmstyle}
{
	usetwoside=false,
	linewidth=3pt,
	backgroundcolor=black!05,
	linecolor=bluegray,
	leftline=true,
	rightline=true,
	bottomline=false,
	topline=false,
	%nobreak=true,
	splittopskip=2\topsep,
	afterlastframe={\vspace{\topsep}}
}

\mdfdefinestyle{probstyle}
{
	usetwoside=false,
	linewidth=3pt,
	backgroundcolor=red!05,
	linecolor=redgray,
	leftline=true,
	rightline=true,
	bottomline=false,
	topline=false,
	%nobreak=true,
	splittopskip=2\topsep,
	afterlastframe={\vspace{\topsep}}
}

%% Theorem Counter
\newcounter{thmcounter}
\def\newthm#1#2{
	\newmdtheoremenv[ntheorem,style=thmstyle]{#1}[thmcounter]{#2}
	\newmdtheoremenv*[ntheorem,style=thmstyle]{#1-nn}{#2}
}
\def\newprob#1#2{
	\newmdtheoremenv[ntheorem,style=probstyle]{#1}[thmcounter]{#2}
	\newmdtheoremenv*[ntheorem,style=probstyle]{#1-nn}{#2}
}
\def\newdef#1#2{\newtheorem{#1}[thmcounter]{#2}\newtheorem*{#1-nn}{#2}}
\theoremseparator{.}
\numberwithin{thmcounter}{chapter}
\renewcommand*{\thechapter}{\arabic{chapter}}
\renewcommand*{\thesection}{\arabic{section}}
%\renewcommand*{\thethmcounter}{\arabic{chapter}.\arabic{section}.\arabic{thmcounter}}
\renewcommand*{\thethmcounter}{\arabic{chapter}.\arabic{thmcounter}}

%% Sätze
\theorembodyfont{\normalfont}
\theoremheaderfont{\bfseries}
\theoremstyle{plain}

\newthm{satz}{Satz}
\newthm{theorem}{Theorem}
\newthm{lemma}{Lemma}
\newthm{korollar}{Korollar}
\newthm{folgerung}{Folgerung}
\newthm{hilfssatz}{Hilfssatz}
\newthm{proposition}{Proposition}
\newthm{satz-definition}{Definition und Satz}

%% Notationen, Bemerkungen, Beispiele, etc.
\theorembodyfont{\normalfont}

\newthm{bezeichnung}{Bezeichnung}
\newthm{bezeichnungen}{Bezeichnungen}
\newthm{voraussetzung}{Voraussetzung}
\newthm{voraussetzungen}{Voraussetzung}
\newthm{bemerkung}{Bemerkung}
\newthm{bemerkungen}{Bemerkungen}
\newthm{definition}{Definition}
\newthm{definitionen}{Definitionen}
\newthm{notation}{Notation}
\newthm{erinnerung}{Erinnerung}
\newthm{frage}{Frage}
\newdef{beispiel}{Beispiel}
\newdef{beispiele}{Beispiele}
\newdef{konstruktion}{Konstruktion}
\newdef{konstruktionen}{Konstruktionen}

%% Probleme / Warnungen
\newprob{ziel}{Ziel}
\newprob{ziele}{Ziele}
\newprob{problem}{Problem}
\newprob{probleme}{Probleme}
\newprob{warnung}{Warnung}
\newprob{warnungen}{Warnungen}
\newprob{achtung}{Achtung}
\newprob{vermutung}{Vermutung}
\newprob{vermutungen}{Vermutungen}

%% Beweise
\newtheoremstyle{mynonumberplain}%
	{\item[\theorem@headerfont\hskip\labelsep ##1\theorem@separator]}
	{\item[\theorem@headerfont\hskip\labelsep ##3\theorem@separator]}
\theoremstyle{mynonumberplain}
\theoremheaderfont{\itshape}
\theoremindent=0pt
\theorembodyfont{\normalfont}
\theoremseparator{.}
\theoremsymbol{\scalebox{0.8}{\ensuremath{\blacksquare}}}

\newtheorem{proof}{Beweis}
\newtheorem{beweisidee}{Beweisidee}
\newtheorem{beweis}{Beweis}
%\RequirePackage{environ}
%\NewEnviron{beweis}[1][]{}

\endinput
