
\chapter{Cartan-Zerlegung}
\label{chapter:cartan-decomposition}
\setStdField{K}
Sei \(K\) algebraisch abgeschlossener Körper der Charakteristik \(0\).
Alle hier betrachteten \(K\)-Vektorräume seien ferner als endlich"=dimensional vorausgesetzt.

\section{Wurzelraum-Zerlegung}
\begin{beispiel}[Modell für Wurzelraum-Zerlegung]
	Die Lie-Algebra \(L \coloneq \sln{K}\) mit \(n \geq 2\) ist einfach.
	Sei \(e_{i,j} = \Kronecker{-}{i,j}\) für \(1 \leq i, j \leq n\) die Standardbasis von \(K^{n \times n}\) und
	\begin{align*}
		H \coloneq \{ \underbrace{\begin{pmatrix}
			\lambda_1 & & 0 \\
			& \ddots & \\
			0 & & \lambda_n
		\end{pmatrix}}_{= \sum_{i=1}^{n} \lambda_i e_{i,i} } : \sum_{i=1}^{n} \lambda_i = 0 \} .
	\end{align*}
	\(H\) ist abelsche Unteralgebra von \(L\).
	\(L\) hat die Basis
	\begin{itemize}
		\item \(e_{i,j}\) für \(1 \leq j < i \leq n\) (\Def{negative Wurzeln})
		\item \(e_{i,j}\) für \(1 \leq i < j \leq n\) (\Def{positive Wurzeln})
		\item \(e_{i,i} - e_{n,n}\) für \(1 \leq i \leq n-1\).
	\end{itemize}
	Setzen wir \(L_{i,j} = \VecSpan{e_{i,j}}\) für \(1 \leq i \neq j \leq n\), dann gilt
	\[ L = \pr{ \bigoplus_{i > j} L_{i,j} } \oplus H \oplus \pr{ \bigoplus_{i < j} L_{i,j} } \]
	als \(K\)-Vektorraum.
	Da \(H\) abelsch ist, genügt für die Beschreibung der Lie-Klammer von \(L\)
	\begin{enumerate}
		\item \([h, e_{i,j}]\) für \(h \in H\) und \(1 \leq i \neq j \leq n\) und
		\item \([e_{i,j}, e_{k,e}]\) für \(1 \leq i \neq j \leq n, 1 \leq k \neq l \leq n\)
	\end{enumerate}
	zu bestimmen.
	\begin{enumerate}
		\item Sei \(h = \sum_{k=1}^{n} \lambda_k e_{k,k} \in H\), dann
			\begin{align*}
				[h, e_{i,j}]
				&= \sum_{k=1}^{n} \lambda_k ( e_{k,k} e_{i,j} - e_{i,j} e_{k,k} ) \\
				&= \lambda_i e_{i,j} - \lambda_j e_{i,j} \\
				&= (\lambda_i - \lambda_j) e_{i,j}
			\end{align*}
			\(e_{i,j}\) ist simultaner Eigenvektor für \(\Ad~h\) mit Eigenwert \(\alpha_{i,j}(h) = \lambda_i - \lambda_j\).
			Wir erhalten die lineare Abbildung
			\[ \alpha_{i,j} : H \to K, ~ \sum_{k=1}^{n} \lambda_k e_{k,k} \mapsto \lambda_i - \lambda_j \]
			und es ist daher \(\alpha_{i,j} \in \Dual{H} = \Hom{H}{K}\).
		\item Es gilt
			\begin{align*}
				[e_{i,j}, e_{k,l}]
				&= e_{i,j} e_{k,l} - e_{k,l} e_{i,j} \\
				&= \Kronecker{j}{k} e_{i,l} - \Kronecker{l}{i} e_{k,j} \\
				&= \begin{cases}
					0, & \text{falls } j \neq k \text{ und } l \neq i \\
					e_{i,l}, & \text{falls } j = k \text{ und } l \neq i \\
					- e_{k,j}, & \text{falls } j \neq k \text{ und } l = i \\
					e_{i,i} - e_{j,j}, & \text{falls } j = k \text{ und } l = i
				\end{cases}
				.
			\end{align*}
			Unmittelbar ergibt sich daraus
			\begin{align*}
				[L_{i,j}, L_{k,l}] &= 0, \text{ falls } j \neq k \text{ und } l \neq i \\
				[L_{i,j}, L_{k,l}] &= L_{i,l}, \text{ falls } l \neq i \\
				[L_{i,j}, L_{k,l}] &= L_{k,j}, \text{ falls } k \neq j
			\end{align*}
			und \([L_{i,j}, L_{k,l}] \in H\).
			Weiter gilt \(\alpha_{i,j} + \alpha_{j,l} = \alpha_{i,l}\).
	\end{enumerate}
\end{beispiel}
In der allgemeinen Wurzelraum-Zerlegung wird zunächst \(H\) wie oben durch abstrakte Eigenschaften und die \(e_{i,j}\) als simultane Eigenvektoren der \(\Ad~h\) für \(h \in H\) definiert.
\begin{definition}
	Sei \(L\) Lie-Algebra über \(K\).
	Eine Unteralgebra \(T \SubLieAlg L\) heißt \Def{toral}, falls \(\Ad~t \in \End{L}\) für \(t \in T\) halbeinfach ist (jedes \(t \in T\) ist ad-halbeinfach).
\end{definition}
\begin{lemma}
	\label{thm:toral-subalgebra-is-abelian}
	Sei \(L\) Lie-Algebra über \(K\) und \(T \SubLieAlg L\) torale Unteralgebra, dann ist \(T\) abelsch.
\end{lemma}
\begin{beweis}
	%Sei \(\Ad_T: T \to \End{T}, ~ x \mapsto \Restr{(\Ad~x)}{T}{\End{T}}\).
	Für \(x \in T\) ist \(\Ad~x\) halbeinfach, d.h. diagonalisierbar.
	Nach dem Minimalpolynom-Kriterium ist dann auch \(\Ad[T]~x\) halbeinfach.
	Wir wollen \(\Ad[T]~x = 0\) für alle \(x \in T\) zeigen.
	Dazu genügt, wenn \(\Ad[T]~x\) keine von \(0\) verschiedene Eigenwert besitzt:
	Sei \(0 \neq x \in T\).
	Angenommen, es existieren \(0 \neq y \in T\), \(0 \neq a \in K\) mit \([x,y] = \Ad[T]~x(y) = ay\).
	Setze \(z \coloneq -ay\), dann ist \(\Ad[T]~y(z) = [y,z] = 0\), d.h. \(z\) ist Eigenvektor zum Eigenwert \(0\) von \(\Ad[T]~y\).
	\(\Ad[T]~y\) ist diagonalisierbar, d.h. es existiert eine Basis \(\{ u_1, \ldots, u_m \}\) von \(T\) mit \(\Ad[T]~y(u_j) = b_j u_j, b_j \in K\).
	Sei \(x = \sum_{j=1}^{m} c_j u_j\) mit \(c_j \in K\), dann
	\[ z = -ay = -[x,y] = [y,x] = \Ad[T]~y(x) = \sum_{j=1}^{m} c_j b_j u_j \neq 0 ,\]
	d.h. \(c_k b_k \neq 0\) für ein \(1 \leq k \leq n\).
	Aber dann ist
	\[ 0 = \Ad[T]~y(z) = \sum_{j=1}^{m} \underbrace{c_j b_j^2 u_j}_{\geq 0} \]
	im Widerspruch zu \(c_k b_k^2 \neq 0\).
\end{beweis}
Sei im Folgenden \(L\) halbeinfache Lie-Algebra über \(K\) und \(H\) eine maximale (bezüglich Inklusion) torale Unteralgebra von \(L\).
\begin{definition}
	Für \(\alpha \in \Dual{H} = \Hom{H}{K}\) sei
	\[ L_{\alpha} \coloneq \{ x \in L : [h,x] = \alpha(h)x, ~ \forall h \in H \} .\]
	Ist \(\alpha \neq 0 \) und \(L_{\alpha} \neq 0\), dann heißt \(\alpha \in \Dual{H}\) \Def{Wurzel} von \(L\) bezüglich \(H\) und \(L_{\alpha}\) der zugehörige \Def{Wurzelraum} von \(L\).
\end{definition}
(\(L_{\alpha}\) besteht aus simultanen Eigenvektoren von \(\Ad~h\) zu den Eigenwerten \(\alpha(h)\) für \(h \in H\).)
Für \(\sln[2]{K}\) sind Wurzelräume genau die Gewichtsräume.
Sei nun \(\Phi\) die Menge der Wurzeln von \(L\) bezüglich \(H\).
\begin{satz}[Wurzelraum- bzw. Cartan-Zerlegung von \(L\)]
	\label{thm:cartan-decomposition}
	Sei \(L\) halbeinfache Lie-Algebra über \(K\), \(H\) eine maximale torale Unteralgebra und \(\Phi\) die Menge der Wurzeln von \(L\) bezüglich \(H\).
	Dann ist
	\[ L = \Centralizer{H} \oplus \bigoplus_{\alpha \in \Phi} L_{\alpha} \]
	und insbesondere \(\Phi\) endlich.
\end{satz}
\begin{beweis}
	Nach \cref{thm:toral-subalgebra-is-abelian} ist \(H\) abelsch.
	Dann besteht \(\Ad_L~H \coloneq \{ \Ad_L~h : h \in H \} \subseteq \gl{L}\) aus paarweise"=vertauschbaren, halbeinfachen Endomorphismen von \(L\) (\(\Ad_L\) ist Lie"=Algebren"=Homomorphismus).
	Folglich ist \(\Ad_L~H\) simultan diagonalisierbar und
	\[ L = \bigoplus_{\substack{\alpha \in \Dual{H}\\L_{\alpha} \neq 0}} L_{\alpha} .\]
	Ferner gilt \(L_0 = \{ x \in L : [h,x] = 0, ~ \forall h \in H \} = \Centralizer{H}\).
\end{beweis}
Im Folgenden zeigen wir \(\Centralizer{H} = H\) und untersuchen \(\Phi\).
Wir werden dabei zeigen, dass \(L\) durch \(\Phi\) bis auf Isomorphie bestimmt ist.
\begin{satz}
	\label{thm:rootspace-relations}
	\begin{enumerate}
		\item \([L_{\alpha}, L_{\beta}] \SubLieAlg L_{\alpha + \beta}\) für alle \(\alpha, \beta \in \Dual{H}\).
		\item Für \(0 \neq \alpha \in \Dual{H}\) und \(x \in L_{\alpha}\) ist \(\Ad~x\) nilpotent.
		\item Seien \(\alpha, \beta \in \Dual{H}\) mit \(\alpha + \beta \neq 0\).
			Dann sind \(L_{\alpha} \RelOrtho[\Kill] L_{\beta}\) orthogonal bezüglich der Killing-Form.
	\end{enumerate}
\end{satz}
\begin{beweis}
	\begin{enumerate}
		\item Jacobi-Identität:
			Seien \(x \in L_{\alpha}\), \(y \in L_{\beta}\) und \(h \in H\), dann
			\begin{align*}
				\Ad~h([x,y])
				&= [h,[x,y]] \\
				&= [[h,x],y] + [x,[h,y]] \\
				&= \alpha(h) [x,y] + \beta(h) [x,y] \\
				&= (\alpha + \beta)(h) [x,y] .
			\end{align*}
		\item Sei \(\beta \in \Dual{H}\) und \(y \in L_{\beta}\).
			Aus (1) folgt \((\Ad~x)^n (y) \in L_{n \alpha + \beta}\) für alle \(n \in \Nats\).
			Wähle \(n \in \Nats\) mit \(L_{n \alpha + \beta} = 0\) für alle \(\beta \in \Phi \cup \{0\}\).
			(Da \(L\) endlich"=dimensional ist, existiert ein solches \(n\).)
			Mit \cref{thm:cartan-decomposition} folgt \((\Ad~x)^n = 0\).
		\item Sei \(h \in H\) mit \((\alpha + \beta)(h) \neq 0\).
			Für \(x \in L_{\alpha}, y \in L_{\beta}\) gilt:
			\[ \Kill([h,x],y) = - \Kill([x,h],y) = - \Kill(x, [h,y]) ,\]
			woraus \(\alpha(h) \Kill(x,y) = - \beta(h) \Kill(x,y)\) folgt.
			Daher ist \((\alpha + \beta)(h) \Kill(x,y) = 0\), d.h. \(\Kill(x,y) = 0\).
	\end{enumerate}
\end{beweis}
\begin{korollar}
	Die Einschränkung von \(\Kill\) auf \(\Centralizer{H} = L_0\) ist nicht-ausgeartet.
\end{korollar}
\begin{beweis}
	Sei \(z \in L_0 \cap \Ortho{L_0}\).
	Da insbesondere \(z \in L_0\) ist, folgt \(z \in \Ortho{L_{\alpha}}\) für alle \(\alpha \in \Phi\) nach \cref{thm:rootspace-relations}.
	Nach Vorraussetzung ist \(z \in \Ortho{L_0}\) und mit \cref{thm:cartan-decomposition} folgt \(z \in \Ortho{L} = 0\).
\end{beweis}

%\section{Der Zentralisator von \ensuremath{H}}
\section{Der Zentralisator der maximal toralen Unteralgebra}
Sei \(L\) halbeinfache Lie-Algebra über \(K\) und \(H\) maximale torale Unteralgebra von \(L\).

\begin{lemma}
	\label{thm:product-with-nilpotent}
	Sei \(V\) ein \(K\)-Vektorraum und \(x, y \in \End{V}\) mit \(xy = yx\) und \(y\) nilpotent.
	Dann ist \(xy\) nilpotent.
\end{lemma}
\begin{beweis}
	Es existiert ein \(n \in \Nats\) mit \(y^n = 0\).
	Da \(x\) und \(y\) vertauschen, gilt
	\[ (xy)^n = x^n y^n = x^n 0 = 0 .\]
	(\textit{Alternativ:} \(x,y\) sind simultan trigonalisierbar.)
\end{beweis}
\begin{satz}
	Es ist \(\Centralizer{H} = H\).
\end{satz}
\begin{beweis}
	Es ist \(C \coloneq \Centralizer{H} \supset H\), da \(H\) abelsch ist \cref{thm:toral-subalgebra-is-abelian}.
	\begin{enumerate}[label=\textbf{Schritt \arabic*:}, align=left]
		\item Sei \(x \in C\) und \(x = x_s + x_n\) die abstrakte Jordan"=Chevalley"=Zerlegung von \(x\).
			Dann sind \(x_s, x_n \in C\).
			
			\textbf{Beweis.}
			Da \(x \in C\) folgt \(0 = [x,H] = \Ad~x(H)\).
			Also ist \((\Ad~x)_s(H) = 0 = (\Ad~x)_n(H)\) nach \cref{thm:jordan-chevalley-decomposition}.2.
			Es folgt
			\[ [x_s,H] = \Ad~x_s(H) = 0 = \Ad~x_n(H) = [x_n,H] ,\]
			nach \cref{thm:abstract-jordan-chevalley-decomposition}, \cref{thm:jordan-chevalley-decomposition}.1 und \cref{thm:ad-jordan-chevalley-decomposition}.
			Daher sind \(x_s, x_n \in C\).
		\item Sei \(x \in C\) mit \(\Ad~x\) halbeinfach, dann gilt \(x \in H\).

			\textbf{Beweis.}
			Der \(K\)-Vektorraum \(\VecSpan{H, x}\) ist abelsche Lie-Algebra, also \(\LieSpan{ \Ad~H, \Ad~x }\) ebenso.
			Also ist jedes Element von \(\Ad~\LieSpan{H, x} = \LieSpan{\Ad~H, \Ad~x}\) halbeinfach (Linearkombinationen vertauschbarer halbeinfacher Elemente sind halbeinfach).
			\(\VecSpan{H, x}\) ist also toral und wegen der Maximalität von \(H\) folgt \(x \in H\).

		\item \(\RestrSrc{\Kill}{H \times H}\) ist nicht ausgeartet.

			\textbf{Beweis.}
			Sei \(h \in H \cap \Ortho{H}\).
			Wir zeigen \(\Kill(h,x) = 0\) für alle \(x \in C\), womit \(h = 0\) aus \cref{thm:rootspace-relations} folgt.
			Sei \(x \in C\) und \(x = x_s + x_n\) die abstrakte Jordan"=Chevalley"=Zerlegung von \(x\).
			Nach (1) ist dann \(x_n \in C\), also \([h,x_n] = 0\) und damit \([\Ad~h,\Ad~x_n] = 0\).
			Wegen \cref{thm:product-with-nilpotent} ist dann \(\Kill(h,x_n) = \Trace(\Ad~h \circ \Ad~x_n) = 0\).
			Aus (2) und folgt \(x_s \in H\) und daher ist \(\Kill(h,x_s) = 0\), da \(h \in H \cap \Ortho{H}\).
			Letzlich folgt \(\Kill(h,x) = \Kill(h,x_s) + \Kill(h,x_n) = 0\).

		\item \(C\) ist nilpotent.

			\textbf{Beweis.}
			Sei \(x \in C\) und \(x = x_s + x_n\) die abstrakte Jordan"=Chevalley"=Zerlegung von \(x\).
			Wegen (2) ist \(x_s \in H\) und damit \([x_s, C] = 0\), d.h. \(\Ad_C~x_s = 0\).
			Also ist \(\Ad[C]~x = \Ad[C]~x_n = \Restr{\Ad~x_n}{C}{C}\) nilpotent.
			Wegen \cref{thm:engels-theorem} ist \(C\) also nilpotent.

		\item \(H \cap [C,C] = 0\).

			\textbf{Beweis.}
			Sei \(x \in H \cap [C,C]\) und \(x = \sum_{i=1}^{m} a_i [c_i, c'_i]\), wobei \(a_i \in K\) und \(c_i, c'_i \in C\).
			Für \(h \in H\) gilt
			\[ \Kill(h,x) = \sum_{i=1}^{m} a_i \Kill(h,[c_i,c'_i]) = \sum_{i=1}^{m} \Kill([h,c_i],c'_i) = 0 .\]
			Mit (3) folgt \(x = 0\).

		\item \(C\) ist abelsch.

			\textbf{Beweis.}
			Angenommen, nicht, dann ist \([C,C] \neq 0\).
			Nach (4), \cref{thm:nilpotent-implies-ad-nilpotent}.2 und \cref{thm:engels-theorem}.1 ist dann \(\Center{C} \cap [C,C] \neq 0\).
			Sei \(0 \neq z \in \Center{C} \cap [C,C]\) und \(z = z_s + z_n\) die abstrakte Jordan"=Chevalley"=Zerlegung von \(z\).
			\(\Ad~z_n = (\Ad~z)_n\) ist Polynom (ohne konstanten Term) in \(\Ad~z\).
			Folglich ist \(\Ad[C]~z_n = \Restr{(\Ad~z_n)}{C}{C} = 0\), da \(z \in \Center{C}\), d.h. \(0 = [z,C] = \Ad~z(C)\).
			Also \([z_n,C] = 0\), d.h. \(z_n \in \Center{C}\) (\(z_n \in C\) nach (1)).
			Mit \cref{thm:product-with-nilpotent} folgt \(\Kill(z_n,y) = \Trace(\Ad~z_n \circ \Ad~y) = 0\) für alle \(y \in C\).
			Wegen (3) ist also \(z_n = 0\) und aus (2) und (5) folgt \(z = z_s \in [C,C] \cap H = 0\), Widerspruch.

		\item \(C = H\).

			\textbf{Beweis.}
			Angenommen, nicht.
			Sei \(x \in C \setminus H\) und \(x = x_s + x_n\) die abstrakte Jordan"=Chevalley"=Zerlegung von \(x\).
			Nach (1) und (2) ist dann \(x_n \in C \setminus H\) und \(\Ad~x_n\) nilpotent.
			Dann ist auch \(\Ad~x_n = (\Ad~x)_n\) nilpotent und \(C\) nach (6) abelsch.
			Mit \cref{thm:product-with-nilpotent} folgt
			\[ \Kill(x_n,y) = \Trace(\underbrace{\Ad~x_n \circ \Ad~y}_{\text{nilpotent}}) = 0 \]
			für alle \(y \in C\).
			Wir erhalten den Widerspruch \(x_n = 0\) aus (3).
	\end{enumerate}
\end{beweis}
\begin{korollar}
	\label{thm:simplified-rootspace-decomposition}
	Die Einschränkung von \(\Kill\) auf \(H\) ist nicht-ausgeartet und
	\[ L = H \oplus \pr{ \bigoplus_{\alpha \in \Phi} L_{\alpha} } .\]
\end{korollar}

%\section{Eigenschaften von \ensuremath{\Phi}}
\section{Eigenschaften der Wurzelmenge}
Sei \(L\) halbeinfache Lie-Algebra über \(K\), \(H\) maximale torale Unteralgebra von \(L\) und \(\Phi\) die Menge der Wurzeln von \(L\) bezüglich \(H\).
Es liegt \(\Phi \subseteq \Dual{H}\) außerhalb von \(L\).
Mittels der Killing-Form \(\Kill\) von \(L\) kann \(\Phi\) nach \(L\) zurückgeholt werden.
\begin{bemerkung}
	\label{thm:killing-dual-vector}
	Zu \(\mu \in \Dual{H}\) existiert genau ein \(t_{\mu} \in H\) mit
	\[ \mu(h) = \Kill(t_{\mu}, h) \quad \text{für alle } h \in H .\]
	Die Abbildung \(\Dual{H} \to H, \mu \mapsto t_{\mu}\) ist \(K\)-Vektorraum-Isomorphismus.
\end{bemerkung}
\begin{beweis}
	Sei \(y \in H\).
	Dann ist \(\kappa_y: H \to K, \kappa_y(h) \coloneq \Kill(y,h)\) in \(\Dual{H}\) und \(\RestrSrc{\Kill}{H \times H}\) nicht ausgeartet.
	Dann ist \(H \to \Dual{H}, y \mapsto \kappa_y\) ein Monomorphismus, also Isomorphismus.
	Zu \(\mu \in \Dual{H}\) existiert folglich ein \(t_{\mu}\) mit \(\kappa_{t_{\mu}} = \mu\).
\end{beweis}
\begin{satz}
	\label{thm:special-sub-algebra}
	\begin{enumerate}
		\item \(\Dual{H} = \LieSpan{\Phi}\).
		\item Für \(\alpha \in \Phi\) ist \(- \alpha \in \Phi\).
		\item Seien \(\alpha \in \Phi\), \(x \in L_{\alpha}\) und \(y \in L_{-\alpha}\).
			Dann ist \([x,y] = \Kill(x,y) t_{\alpha}\) (mit \(t_{\alpha}\) wie in \cref{thm:killing-dual-vector}).
		\item Für \(\alpha \in \Phi\) ist \([L_{\alpha}, L_{-\alpha}] = \LieSpan{t_{\alpha}}\).
		\item \(\alpha(t_{\alpha}) = \Kill(t_{\alpha},t_{\alpha}) \neq 0\) für alle \(\alpha \in \Phi\).
		\item Sei \(\alpha \in \Phi\) und \(0 \neq x_{\alpha} \in L_{\alpha}\).
			Dann existiert \(y_{\alpha \in L_{-\alpha}}\), so dass \(h_{\alpha} \coloneq [x_{\alpha},y_{\alpha}] = \frac{2 t_{\alpha}}{\Kill(t_{\alpha},t_{\alpha})}\) und \(S_{\alpha} \coloneq \VecSpan{ x_{\alpha}, y_{\alpha}, h_{\alpha} }\) drei-dimensionale Unteralgebra von \(L\) ist.
			Es existiert ferner ein Lie-Algebren-Isomorphismus \(S_{\alpha} \to \sln[2]{K}\) mit
			\[
				x_{\alpha} \mapsto \begin{pmatrix} 0 & 1 \\ 0 & 0 \end{pmatrix}, \quad
				y_{\alpha} \mapsto \begin{pmatrix} 0 & 0 \\ 1 & 0 \end{pmatrix}, \quad
				h_{\alpha} \mapsto \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}.
			\]
			Weiter gilt \(h_{\alpha} = 2 t_{\alpha} / \Kill(t_{\alpha},t_{\alpha})\) und \(h_{-\alpha} = - h_{\alpha}\).
	\end{enumerate}
\end{satz}
\begin{beweis}
	\begin{enumerate}
		\item Angenommen, \(\LieSpan{\Phi} \subseteq \Dual{H}\).
			Dann existiert \(0 \neq h \in H\) mit \(\alpha(h) = 0\) für alle \(\alpha \in \Phi\).
			Die Definition von \(L_{\alpha}\) liefert \([h,L_{\alpha}] = 0\) für alle \(\alpha \in \Phi\).
			Also ist \([h,L] = 0\) wegen \cref{thm:simplified-rootspace-decomposition} und \([h,H] = 0\).
			Wir erhalten den Widerspruch \(0 \neq h \in \Center{L}\).
		\item Sei \(\alpha \in \Phi\).
			Angenommen, \(- \alpha \not\in \Phi\), also \(\alpha + \beta \neq 0\) für alle \(\beta \in \Phi\).
			Wegen \cref{thm:rootspace-relations}.3 ist \(\Kill(L_{\alpha}, L_{\beta}) = 0\) für \(\beta \in \Dual{H}\).
			Aus \cref{thm:simplified-rootspace-decomposition} folgt \(\Kill(L_{\alpha},L) = 0\), also \(L_{\alpha} \subseteq L \cap \Ortho{L} = 0\), Widerspruch.
		\item Seien \(\alpha \in \Phi\), \(x \in L_{\alpha}\) und \(y \in L_{-\alpha}\).
			Für \(h \in H\) gilt
			\begin{align*}
				\Kill(h,[x,y])
				&= \Kill([h,x],y)
				= \alpha(h) \Kill(x,y) \\
				&= \Kill(t_{\alpha}, h) \Kill(x,y)
				= \Kill(t_{\alpha} \Kill(x,y), h)
				= \Kill(h, \Kill(x,y) t_{\alpha})
				.
			\end{align*}
			Wegen \cref{thm:rootspace-relations}.1 ist \([x,y] \in L_{\alpha + (-\alpha)} = L_0 = H\).
			Aus \cref{thm:simplified-rootspace-decomposition} folgt \([x,y] = \Kill(x,y) t_{\alpha}\).
		\item Sei \(\alpha \in \Phi\), wegen (3) also \([L_{\alpha}, L_{-\alpha}] \subseteq \LieSpan{t_{\alpha}}\).
			Ferner sei \(0 \neq x \in L_{\alpha}\).
			Aus \cref{thm:rootspace-relations}.3 folgt \(\Kill(x,L_{\beta}) = 0\) für \(\beta \in \Dual{H} \setminus \{-\alpha\}\).
			\cref{thm:simplified-rootspace-decomposition} impliziert \(\Kill(x,L_{-\alpha}) \neq 0\), da \(\Kill\) nicht-ausgeartet ist.
			Es existiert also ein \(y \in L_{-\alpha}\) mit \(\Kill(x,y) \neq 0\), nach (3) also \([x,y] = \Kill(x,y) t_{\alpha} \neq 0\).
		\item Sei \(\alpha \in \Phi\).
			Angenommen, \(\alpha(t_{\alpha}) = 0\).
			Seien \(x \in L_{\alpha}\), \(y \in L_{-\alpha}\) mit \([x,y] = t_{\alpha}\) (vgl. Beweis von (4)).
			Dann sind
			\[ [t_{\alpha},x] = \alpha(t_{\alpha}) x = 0, \qquad [t_{\alpha},y] = - \alpha(t_{\alpha}) y = 0 .\]
			Folglich ist \(A \coloneq \VecSpan{x, y, t_{\alpha}}\) eine Lie-Unteralgebra von \(L\) mir \([A,A] = \LieSpan{t_{\alpha}}\) und daher \(A\) auflösbar.
			Entsprechend \cref{thm:lies-theorem} ist \(\Ad~A \SubLieAlg \gl{L}\) isomorph zu einer Lie-Algebra oberer Dreiecksmatrizen.
			Deswegen ist \(\Ad~t_{\alpha} = [\Ad~x,\Ad~y]\) nilpotent.
			Andererseits ist \(\Ad~t_{\alpha}\) halbeinfach, da \(t_{\alpha} \in H\) und \(H\) toral ist.
			Es folgt \(\Ad~t_{\alpha} = 0\) und daher \(t_{\alpha} = 0\), da \(\Ad\) injektiv ist.
			Das ist ein Widerspruch, da \(\alpha = 0\) aus \cref{thm:killing-dual-vector} folgt.
		\item Sei \(\alpha \in \Phi\) und \(0 \neq x_{\alpha} \in L_{\alpha}\).
			Nach (5) ist \(\Kill(t_{\alpha},t_{\alpha}) \neq 0\).
			Sei \(y_{\alpha} \in L_{-\alpha}\) mit \(\Kill(x_{\alpha},y_{\alpha}) = 2 / \Kill(t_{\alpha}, t_{\alpha})\).
			Aus (3) folgt
			\[ h_{\alpha} = [x_{\alpha},y_{\alpha}] = \Kill(x_{\alpha},y_{\alpha}) \cdot t_{\alpha} = \frac{2 t_{\alpha}}{\Kill(t_{\alpha}, t_{\alpha})} .\]
			Weiter gilt
			\[ [h_{\alpha},x_{\alpha}] = \frac{2}{\alpha(t_{\alpha})} \cdot [t_{\alpha},x_{\alpha}] = \frac{2}{\alpha(t_{\alpha})} \cdot \alpha(t_{\alpha}) x_{\alpha} = 2 x_{\alpha} \]
			und analog für \([h_{\alpha},y_{\alpha}] = 2 y_{\alpha}\).
			Wir haben \(\Dim{S_{\alpha}} = 3\), da \(t_{\alpha} \in H = L_0\), \(x_{\alpha} \in L_{\alpha}\) und \(y_{\alpha} \in L_{-\alpha}\).
			Deswegen lässt sich die Zuordnung
			\[
				x_{\alpha} \mapsto \begin{pmatrix} 0 & 1 \\ 0 & 0 \end{pmatrix}, \quad
				y_{\alpha} \mapsto \begin{pmatrix} 0 & 0 \\ 1 & 0 \end{pmatrix}, \quad
				h_{\alpha} \mapsto \begin{pmatrix} 1 & 0 \\ 0 & -1 \end{pmatrix}.
			\]
			zu einem \(K\)-Vektorraum-Isomorphismus \(S_{\alpha} \to \sln[2]{K}\) fortsetzen.
			Dieser ist ein Lie"=Algebren"=Homomorphismus.
			Nach der Definition ist \(t_{-\alpha} = - t_{\alpha}\) und daher
			\begin{align*}
				h_{-\alpha}
				= \frac{ 2 t_{-\alpha} }{ \Kill(t_{-\alpha},t_{-\alpha}) }
				= - \frac{ 2 t_{\alpha} }{ - \alpha(t_{-\alpha}) }
				= - \frac{ 2 t_{\alpha} }{ \alpha(t_{\alpha}) }
				= - \frac{ 2 t_{\alpha} }{ \Kill(t_{\alpha},t_{\alpha}) }
				= - h_{\alpha}
				.
			\end{align*}
	\end{enumerate}
\end{beweis}

\section{Integralität}
Sei wieder \(K\) ein algebraisch-abgeschlossener Körper der Charakteristik \(0\), \(L\) eine halbeinfache Lie-Algebra über \(K\) und \(\Phi\) die Menge der Wurzeln von \(L\) bezüglich \(H\).
\[ L = H \oplus \pr{ \bigoplus_{\alpha \in \Phi} L_{\alpha} } \]
Zu \(\alpha \in \Phi\) existieren \(x_{\alpha} \in L_{\alpha}\), \(y_{\alpha} \in L_{-\alpha}\) mit
\[ h_{\alpha} \coloneq [x_{\alpha}, y_{\alpha}] = \frac{2 t_{\alpha}}{\Kill(t_{\alpha},t_{\alpha})} ,\]
und ferner ist
\[ S_{\alpha} = \VecSpan{x_{\alpha}, y_{\alpha}, h_{\alpha}} \LieIso \sln[2]{K} \]
als Lie-Algebra.
\(L\) kann als \(S_{\alpha}\)-Modul durch
\[ \RestrSrc{\Ad}{S_{\alpha}}: S_{\alpha} \to \gl{L} \]
aufgefasst werden.
Nach unseren Untersuchungen in Kapitel 2, \cref{section:finite-representation-of-sl2K} sind die \(S_{\alpha} \LieIso \sln[2]{K}\)-Moduln bekannt.
Das machen wir uns in dem folgenden Satz zunutze.
Dabei spielen besonders die Gewichtsräume wieder eine Rolle, ferner sind \cref{thm:base-products}, \cref{thm:characterization-of-simple-L-modules} und \cref{thm:h-eigenvalues-are-integers} essentiell.

\begin{satz}
	\label{thm:root-calculations}
	\begin{enumerate}
		\item Für \(\alpha \in \Phi\) ist \(\Dim{L_\alpha} = 1\).
			Insbesondere ist \(S_{\alpha} = \RootSpace{\alpha} + \RootSpace{-\alpha} + H_{\alpha}\) mit \(H_{\alpha} = [\RootSpace{\alpha}, \RootSpace{-\alpha}]\) und zu jedem \(0 \neq x_{\alpha} \in \RootSpace{\alpha}\) existiert genau ein \(y_{\alpha} \in \RootSpace{-\alpha}\) mit \([x_{\alpha},y_{\alpha}] = h_{\alpha} = \frac{2 t_{\alpha}}{\Kill(t_{\alpha},t_{\alpha})}\).
		\item Für \(\alpha \in \Phi\) und \(s \in K\) mit \(s \alpha \in \Phi\) ist \(s \in \{-1, 1\}\).
		\item Für \(\alpha, \beta \in \Phi\) ist \(\beta(h_{\alpha}) \in \Ints\) und \(\beta - \beta(h_{\alpha}) \alpha \in \Phi\).
		\item Für \(\alpha, \beta \in \Phi\) mit \(\alpha + \beta \in \Phi\) folgt \([\RootSpace{\alpha}, \RootSpace{\beta}] = \RootSpace{\alpha + \beta}\).
		\item Seien \(\alpha, \beta \in \Phi\) und \(\beta \neq \pm \alpha\).
			Seien ferner \(r\) bzw.\ \(q\) die größten ganzen Zahlen, so dass \(\beta - r \alpha \in \Phi\) bzw.\ \(\beta + q \alpha \in \Phi\) noch Wurzeln sind.
			Dann ist \(\beta + i \alpha \in \Phi\) für alle \(-r \leq i \leq q\) und \(\beta(h_{\alpha}) = r - q\).
		\item \(L\) wird als Lie-Algebra von \(\RootSpace{\alpha}, \alpha \in \Phi\) erzeugt.
	\end{enumerate}
\end{satz}
\begin{beweis}
	Wir beweisen bestimmte Teile zusammen:
	\begin{enumerate}
		\item[(1),(2):]
			Sei \(S_{\alpha}\) wie in \cref{thm:special-sub-algebra}.6.
			\(L\) ist \(S_{\alpha}\)-Modul durch \(x \cdot v \coloneq [x,v], x \in S_{\alpha}, v \in L\).
			Setze
			\[ M \coloneq \VecSpanRel{H \cdot \RootSpace{c \alpha}}{c \in \Units{K}} = \VecSpanRel{H \cdot \RootSpace{c \alpha}}{c \in K} ,\]
			da \(\RootSpace{0} = \Centralizer{H} = H\).
			Es ist \(S_{\alpha} = \LieSpan{x_{\alpha}, y_{\alpha}, z_{\alpha}}\) mit \(x_{\alpha} \in \RootSpace{\alpha}, y_{\alpha} \in \RootSpace{-\alpha}\) und \(h_{\alpha} = [x_{\alpha},y_{\alpha}] = \frac{2 t_{\alpha}}{\Kill(t_{\alpha},t_{\alpha})}\).

			Sei \(c \in K\) und \(v \in \RootSpace{c \alpha}\).
			Aus \cref{thm:rootspace-relations} und der Wurzelraum-Definition ergibt sich
			\begin{align*}
				[x_{\alpha},v] & \in \RootSpace{c \alpha + \alpha} = \RootSpace{(c + 1) \alpha} \subseteq M
				[y_{\alpha},v] & \in \RootSpace{c \alpha - \alpha} = \RootSpace{(c - 1) \alpha} \subseteq M
				[h_{\alpha},v] & \in \RootSpace{c \alpha}
				.
			\end{align*}
			Folglich ist \(M\) ein \(S_{\alpha}\)-Untermodul von \(L\).
			Wegen \(h_{\alpha} \in H\) sind die Gewichtsräume von \(M\) gerade die Vektorräume \(\RootSpace{0} = H\) und \(\RootSpace{c \alpha}\) mit \(c \alpha \in \Phi\).

			Sei \(c \neq 0\), so dass \(c \alpha \in \Phi\), d.h. \(\RootSpace{c \alpha} \neq 0\).
			Mit \(v \in \RootSpace{c \alpha}\) ergibt sich
			\[
				h_{\alpha} \cdot v
				= [h_{\alpha},v]
				= c \underbrace{\alpha(h_{\alpha})}_{= 2 \text{, da } \alpha(t_{\alpha}) = \Kill(t_{\alpha},t_{\alpha}) } v
				= 2 c v
				.
			\]
			Das Gewicht von \(h_{\alpha}\) auf \(\RootSpace{c \alpha}\) beträgt also \(2c\) (\(\star\)).
			Aus \cref{thm:h-eigenvalues-are-integers} folgt \(2c \in \Ints\) bzw. \(c \in \frac{1}{2} \Ints\).

			Sei \(h \in \Kernel{\alpha} \SubVec H\), dann
			\begin{align*}
				x_{\alpha} \cdot h & = [x_{\alpha},h] = - [h,x_{\alpha}] = - \alpha(h) x_{\alpha} = 0 \\
				y_{\alpha} \cdot h & = [y_{\alpha},h] = - [h,y_{\alpha}] = - \alpha(h) y_{\alpha} = 0 \\
				h_{\alpha} \cdot h & = [h_{\alpha},h] = - [h,h_{\alpha}] = - \alpha(h) h_{\alpha} = 0
				.
			\end{align*}
			Also ist \(\Kernel{\alpha}\) ein \(S_{\alpha}\)-Untermodul von \(M\).
			Auch \(S_{\alpha}\) ist \(S_{\alpha}\)-Untermodul von \(M\) und \(H \SubLieAlg \Kernel{\alpha} + S_{\alpha}\), da \(H = \Kernel{\alpha} + \LieSpan{h_{\alpha}}\).
			Somit gilt \(M = (\Kernel{\alpha} + S_{\alpha}) \oplus M'\) mit \(S_{\alpha}\)-Untermodul \(M'\) von \(M\) nach dem \cref{thm:engels-theorem}.
			Es ist \(c \alpha(h_{\alpha}) = 2c\) und daher \(\WeightSpace{M}{0} = H \SubLieAlg \Kernel{\alpha} + S_{\alpha}\) der Gewichtsraum von \(M\) zu dem Gewicht \(0\).
			Daher sind die Gewichte von \(h_{\alpha}\) in \(M'\) ungerade (Ein einfacher \(S_{\alpha}\)-Untermodul von \(M'\) mit geradem Gewicht hätte nach \cref{thm:characterization-of-simple-L-modules} auch das Gewicht \(0\)).
			Die geraden Gewichte von \(h_{\alpha}\) in \(M\) stammen also von Gewichtsräume in \(\Kernel{\alpha} + S_{\alpha}\) (Gewichte \(0\) und \(\pm 2\)).
			Gerade Gewichte von \(h_{\alpha}\) in \(M\) sind also nur \(0, \pm2\).
			\(2 \alpha\) ist keine Wurzel, sonst gäbe es ein Gewicht \(4\).
			Damit ist \(\beta = \frac{1}{2} \alpha \neq \Phi\), da (\(\star\)) und sonst \(\alpha = 2 \beta \in \Phi\).
			Insbesondere ist \(1\) kein Gewicht von \(h_{\alpha}\) in \(M\).
			Wegen \cref{thm:characterization-of-simple-L-modules} ist \(M' = 0\), d.h. \(M = H + S_{\alpha}\).
			Insbesondere ist \(\Dim{\RootSpace{\alpha}} = 1\), denn \(\RootSpace{\alpha} \subseteq M\) und \(H \cap \RootSpace{\alpha} = 0\).
			Daraus folgt (1) und \(S_{\alpha} = \RootSpace{\alpha} + \RootSpace{- \alpha} + H_{\alpha}\).
			Die Gewichte von \(h_{\alpha}\) in \(M\) sind \(0, +2, -2\).
			Wegen (\(\star\)) ist also \(c \alpha\) genau dann Wurzel, wenn \(c = \pm 1\), d.h. (2) gilt.

		\item[(3),(4),(5):]
			Sei \(\beta \in \Phi\) mit \(\beta \neq \pm \alpha\) (sonst gilt (3) trivialerweise) und
			\[ V = \sum_{i \in \Ints} \RootSpace{\beta + i \alpha} .\]
			Dann ist \(V\) ein \(S_{\alpha}\) Untermodul von \(L\) (Beweis wie für \(M\)).
			Weiterhin ist \(\beta + i \alpha \neq 0\) für alle \(i \in \Ints\) wegen (b), \cref{thm:special-sub-algebra} und \(\beta \neq \pm \alpha\).
			Falls \(\beta + i \alpha \in \Phi\), dann ist \(\RootSpace{\beta + i \alpha}\) ein Gewichtsraum von \(V\) zum Gewicht \(\beta(h_{\alpha}) + 2i\), da
			\[ (\beta + i \alpha)(h_{\alpha}) = \beta(h_{\alpha}) + i \alpha(h_{\alpha}) = \beta(h_{\alpha}) + 2i .\]
			Aus (1) und \cref{thm:h-eigenvalues-are-integers} folgt \(\beta(h_{\alpha}) \in \Ints\), die Gewichtsräume von \(V\) sind eindimensional und entweder nur \(0\) oder nur \(1\) ist ein Gewicht von \(h_{\alpha}\) in \(V\).
			Also ist \(\Dim{\RootSpace[V]{0}} + \Dim{\RootSpace[V]{1}} = 1\).
			\cref{thm:h-eigenvalues-are-integers} besagt \(V\) ist ein einfacher \(S_{\alpha}\)-Modul.
			
			Sind \(q, r\) wie in (5), dann folgt:
			Das höchste Gewicht von \(h_{\alpha}\) in \(V\) ist \(\beta(h_{\alpha}) + 2q\) und das niedrigste \(\beta(h_{\alpha}) - 2r\).
			Wegen \cref{thm:characterization-of-simple-L-modules} sind die Gewicht von \(h_{\alpha}\) durch \(\beta(h_{\alpha}) + 2i\) mit \(-r \leq i \leq q\) gegeben.
			Insbesondere ist \(\beta + i \alpha \in \Phi\) für \(-r \leq i \leq q\).
			Weiter gilt \(-(\beta(h_{\alpha}) + 2q) = \beta(h_{\alpha}) - 2r\) nach \cref{thm:characterization-of-simple-L-modules} und es folgt \(\beta(h_{\alpha}) = r - q\), d.h. (5) gilt.
			Schließlich gilt (c), denn \(\beta - \beta(h_{\alpha} \alpha = \beta + (q - r) \alpha\) und \(-r \leq q-r \leq q\).
			
			Aus \cref{thm:base-products} und \cref{thm:characterization-of-simple-L-modules}, angewandt auf \(V\), folgt:
			Sind \(\mu, \mu + 2\) Gewichte von \(h_{\alpha}\) in \(V\), so gilt \(x_{\alpha} \cdot \RootSpace[V]{\mu} = \RootSpace[V]{\mu + 2}\).
			Wende diese Aussage auf \(\mu = \beta(h_{\alpha})\) an, also \(\RootSpace[V]{\mu} = \RootSpace{\beta}\), \(\RootSpace[V]{\mu + 2} = \RootSpace{\beta + \alpha}\), daraus folgt (4).

		\item [(6):]
			Es ist \(H = \LieSpanRel{t_{\alpha}}{\alpha \in \Phi}\) wegen \cref{thm:killing-dual-vector} und \cref{thm:special-sub-algebra}.1.
			Also
			\[ H = \sum_{\alpha \in \Phi} [\RootSpace{\alpha},\RootSpace{-\alpha}] .\]
	\end{enumerate}
\end{beweis}
\begin{definition}
	Die ganzen Zahlen \(\beta(h_{\alpha})\) für \(\alpha, \beta \in \Phi\) heißen die \Def{Cartan-Invarianten} von \(\Phi\).
\end{definition}

\section{Das Wurzelsystem}
Die Bezeichnungen seien wie im vorigem Abschnitt.
Wir wollen nun ein geeignetes Skalarprodukt für weitere Untersuchungen einführen.
Die Fruchtbarkeit dieses Ansatzes sollte bereits aus vorherigen Abschnitten und der linearen Algebra bekannt sein.

\begin{bemerkung}
	\label{thm:dual-non-degenerate-bilinearform}
	\begin{enumerate}
		\item Die Abbildung
			\[ \DotProd{-}{=}: \Dual{H} \times \Dual{H} \to K, ~ \DotProd{\mu}{\nu} \mapsto \Kill(t_{\mu},t_{\nu}) \]
			ist eine nicht-ausgeartete Bilinearform auf \(\Dual{H}\).
		\item Wir setzen
			\[ E_{\Rats} \coloneq \sum_{\alpha \in \Phi} \Rats \alpha = \VecSpanRel[\Rats]{\alpha}{\alpha \in \Phi} \SubVec[\Rats] \Dual{H} ,\]
			das ist der von \(\Phi\) erzeugte \(\Rats\)-Untervektorraum von \(\Dual{H}\).
	\end{enumerate}
\end{bemerkung}
\begin{satz}
	\label{thm:dual-dot-product}
	\begin{enumerate}
		\item Es gilt \(\Phi \subseteq E_{\Rats}\) und \(\Dim{\Dual{H}} = \Dim[\Rats]{E_{\Rats}}\).
		\item Für \(\mu, \nu \in E_{\Rats}\) ist \(\DotProd{\mu}{\nu} \in \Rats\) und
			\[ \DotProd{-}{=}_{E_{\Rats}} \coloneq \Restr{\DotProd{-}{=}}{E_{\Rats} \times E_{\Rats}}{\Rats} \] % : E_{\Rats} \times E_{\Rats} \to \Rats
			ist positiv definit, d.h. \(\DotProd{\mu}{\mu} > 0 \) für alle \(0 \neq \mu \in E_{\Rats}\).
	\end{enumerate}
\end{satz}
\begin{beweis}
	\begin{enumerate}
		\item Wegen \(\VecSpan{\Phi} = \Dual{H}\) nach \cref{thm:special-sub-algebra}.1 existieren \(\alpha_1, \ldots, \alpha_l \in \Phi\), so dass \(\{ \alpha_1, \ldots, \alpha_l \}\) eine \(K\)-Basis von \(\Dual{H}\) ist.
			Sei \(\beta \in \Phi\).
			Mit geeigneten \(c_i \in K, 1 \leq i \leq l\) ist \(\beta = \sum_{i=1}^{l} c_i \alpha_i\) und daher
			\[ \DotProd{\beta}{\alpha_j} = \sum_{i=1}^{l} (\alpha_i,\alpha_j) c_i \qquad \forall 1 \leq j \leq l .\]
			Nach \cref{thm:special-sub-algebra}.5 ist \(\DotProd{\alpha_j}{\alpha_j} = \Kill(t_{\alpha_j},t_{\alpha_j}) \neq 0\), also
			\[ \frac{2 \DotProd{\beta}{\alpha_j}}{\DotProd{\alpha_j}{\alpha_j}} = \sum_{i=1}^{l} \underbrace{\frac{2 \DotProd{\alpha_i}{\alpha_j}}{\DotProd{\alpha_j}{\alpha_j}}}_{\in \Ints} c_i \]
			für \(1 \leq j \leq l\).
			Denn:
			\[
				\frac{2 \DotProd{\beta}{\alpha_j}}{\DotProd{\alpha_j}{\alpha_j}}
				= \frac{2 \Kill(t_{\alpha_i},t_{\alpha_j})}{\Kill(t_{\alpha_j},t_{\alpha_j})}
				\quad\qquad \textover{\cref{thm:killing-dual-vector}}= \quad\qquad \frac{2 \alpha_i(t_{\alpha_j})}{\Kill(t_{\alpha_j},t_{\alpha_j})}
				\quad \textover{\cref{thm:special-sub-algebra}.5}= \quad \alpha_i(h_{\alpha_j})
				\in \Ints
			\]
			Ferner ist \(((\alpha_i,\alpha_j))_{1 \leq i, j \leq l} \in K^{l \times l}\) invertierbar, da \(\DotProd{-}{=}\) nicht-ausgeartet ist.
			Also ist
			\[ \pr{\frac{2 \DotProd{\alpha_i}{\alpha_j}}{\DotProd{\alpha_j}{\alpha_j}}}_{1 \leq i, j \leq l} \in \Ints^{l \times l} \subseteq \Rats^{l \times l} \]
			invertierbar mit zugehöriger inversen Matix
			%\[ \DotProd{\alpha_i}{\alpha_j} \cdot \DiagMat{2 \DotProd{\alpha_1}{\alpha_1}^{-1}, \ldots, 2 \DotProd{\alpha_l}{\alpha_l}^{-1}} \in \Rats^{l \times l}.\]
			\[
				\DotProd{\alpha_i}{\alpha_j} \cdot \begin{pmatrix}
					2 \DotProd{\alpha_1}{\alpha_1}^{-1} & & 0 \\
					& \ddots & \\
					0 & & 2 \DotProd{\alpha_l}{\alpha_l}^{-1}
				\end{pmatrix} \in \Rats^{l \times l}.
			\]
			Folglich ist \(c_i \in \Rats\) für \(1 \leq i \leq l\), d.h. \(\beta \in E_{\Rats}\) und \(\{\alpha_1, \ldots, \alpha_l\}\) ist \(\Rats\)-Basis von \(E_{\Rats}\).
		\item Seien zunächst \(\mu, \nu \in \Phi\).
			Es gilt
			\[
				\DotProd{\mu}{\nu} = \Kill(t_{\mu},t_{\nu}) = \Trace(\underbrace{\Ad~t_{\mu} \circ \Ad~t_{\nu}}_{\in \End{L}}),
				\qquad
				L = H \oplus \pr{ \bigoplus_{\alpha \in \Phi} \RootSpace{\alpha} }
				.
			\]
			Seien \(\alpha \in \Phi\) und \(x \in \RootSpace{\alpha}\), wegen \(\Ad~t_{\mu}(H) = [t_{\mu},H] = 0\) folgt
			\[
				\Ad~t_{\mu}(\Ad~t_{\nu}(x))
				= \Ad~t_{\mu}([t_{\nu},x])
				= \Ad~t_{\mu}(\alpha(t_{\nu}) x)
				= \alpha(t_{\nu}) [t_{\mu},x]
				= \alpha(t_{\nu}) \alpha(t_{\mu}) x
				.
			\]
			Mit \cref{thm:killing-dual-vector} folgt
			\begin{align}
				\label{equation:dot-product-representation-root-case}
				\DotProd{\mu}{\nu}
				= \sum_{\alpha \in \Phi} \alpha(t_{\mu}) \alpha(t_{\nu})
				\qquad\qquad\textover{\(\alpha(t_{\mu}) = \Kill(t_{\alpha},t_{\mu}) = \DotProd{\alpha}{\mu}\)}=
				\qquad\qquad \sum_{\alpha \in \Phi} \DotProd{\alpha}{\mu} \cdot \DotProd{\alpha}{\nu}
				.
			\end{align}
			Mit \(\mu = \nu = \beta \in \Phi\) folgt
			\[ \DotProd{\beta}{\beta} = \sum_{\alpha \in \Phi} \DotProd{\alpha}{\beta}^2 \]
			und wegen \(\DotProd{\beta}{\beta} \neq 0\) erhalten wir daraus
			\[
				\frac{1}{\DotProd{\beta}{\beta}} = \sum_{\alpha \in \Phi} \pr{
					\frac{1}{2} \underbrace{\frac{2 \DotProd{\alpha}{\beta}}{\DotProd{\beta}{\beta}}}_{\in \Ints}
				}^2 \in \Rats .
			\]
			Für alle \(\beta \in \Phi\) ist daher \(\DotProd{\beta}{\beta} \in \Rats\).
			Da \(\frac{2 \DotProd{\alpha}{\beta}}{\DotProd{\beta}{\beta}} \in \Ints\), ist \(\DotProd{\alpha}{\beta} \in \Rats\) für alle \(\alpha, \beta \in \Phi\).
			Folglich ist \(\DotProd{-}{=}_{E_{\Rats}}\) nicht-ausgeartete Bilinearform auf \(E_{\Rats}\).

			Nun sei \(\{\alpha_1, \ldots, \alpha_l\} \subseteq \Phi\) eine Basis von \(E_{\Rats}\) und \(c_i, d_i \in \Rats, 1 \leq i, \leq l\), sowie
			\[
				\mu \coloneq \sum_{i=1}^{l} c_i \alpha_i
				\qquad
				\nu \coloneq \sum_{i=1}^{l} d_i \alpha_i
				.
			\]
			Daraus ergibt sich
			\begin{nalign}
				\label{equation:dot-product-representation-general-case}
					\DotProd{\mu}{\nu}
					&= \sum_{i=1}^{l} \sum_{j=1}^{l} c_i d_j \DotProd{\alpha_i}{\alpha_j} \\
					&\textover{(\ref{equation:dot-product-representation-root-case})}= \sum_{\alpha \in \Phi} \sum_{i=1}^{l} \sum_{j=1}^{l} \DotProd{\alpha}{\alpha_j} \DotProd{\alpha}{\alpha_j} \\
					&= \sum_{\alpha \in \Phi} \pr{ \sum_{i=1}^{l} c_i \DotProd{\alpha}{\alpha_i} } \pr{ \sum_{j=1}^{l} d_j \DotProd{\alpha}{\alpha_j} } \\
					&= \sum_{\alpha \in \Phi} \DotProd{\alpha}{\mu} \DotProd{\alpha}{\nu}
					,
			\end{nalign}
			wobei \cref{equation:dot-product-representation-root-case} bisher nur für \(\mu, \nu \in \Phi\) gezeigt war.

			Sei nun \(0 \neq \mu \in E_{\Rats}\), dann existiert ein \(\alpha \in \Phi\) mit \(\DotProd{\alpha}{\mu} \neq 0\), da \(\VecSpan[\Rats]{\Phi} = E_{\Rats}\) und \(\DotProd{-}{=}_{E_{\Rats}}\) nicht-ausgeartet ist.
			Es folgt
			\[ \DotProd{\mu}{\mu} \textover{(\ref{equation:dot-product-representation-general-case})}= \sum_{\alpha \in \Phi} \DotProd{\alpha}{\mu}^2 > 0 ,\]
			weshalb \(\DotProd{-}{=}_{E_{\Rats}}\) positiv definit ist.
	\end{enumerate}
\end{beweis}
\begin{definition}
	\label{def:continued-dot-product}
	Sei \(E_{\Rats}\) wie in \cref{thm:dual-dot-product}.
	Setze \(E \coloneq E_{\Rats} \otimes_{\Rats} \Reals\) und setze \(\DotProd{-}{=}_{E_{\Rats}}\) aus \cref{thm:dual-dot-product} bilinear auf \(E \times E\) zu \(\DotProd{-}{=}: E \times E \to \Reals\) fort.
\end{definition}

\section{Zusammenfassung}
Es seien wie zuvor
\begin{itemize}
	\item \(K\) algebraisch-abgeschlossener Körper der Charakteristik \(0\)
	\item \(L\) eine halbeinfache Lie-Algebra über \(K\)
	\item \(H\) eine maximale torale Unteralgebra von \(L\)
	\item \(\Phi\) die Menge der Wurzeln von \(L\) bezüglich \(H\)
	\item \(E_{\Rats} \coloneq \VecSpanRel[\Rats]{\alpha}{\alpha \in \Phi} \subseteq \Dual{H}\)
	\item \(E \coloneq E_{\Rats} \otimes_{\Rats} \Reals\)
\end{itemize}
Auf \(E\) ist eine symmetrische, positiv definite Bilinearform \(\DotProd{-}{=}\), also ein Skalarprodukt womit \((E, \DotProd{-}{=})\) zu einem euklidischem Raum wird, definiert, so dass mit \(\Phi \subseteq E\) gilt
\begin{enumerate}
	\item \(\Phi\) ist endlich, \(E = \VecSpan[\Reals]{\Phi}\), \(0 \neq \Phi\)
	\item für \(\alpha \in \Phi, r \in \Reals\) mit \(r \alpha \in \Phi\) ist \(r = \pm 1\)
	\item für \(\alpha, \beta \in \Phi\) ist \(\beta - \frac{2 \DotProd{\beta}{\alpha}}{\DotProd{\alpha}{\alpha}} \cdot \alpha \in \Phi\)
	\item für \(\alpha, \beta \in \Phi\) ist \(\frac{2 \DotProd{\beta}{\alpha}}{\DotProd{\alpha}{\alpha}} \in \Ints\)
\end{enumerate}
\begin{beweis}
	\begin{enumerate}
		\item \cref{thm:dual-non-degenerate-bilinearform} und \cref{def:continued-dot-product}.
		\item \cref{thm:dual-dot-product}.1 und \cref{thm:root-calculations}.2.
		\item \cref{thm:root-calculations}.3 und Definition des Skalarprodukt \(\DotProd{-}{=}\).
		\item \cref{thm:root-calculations}.3 und Definition des Skalarprodukt \(\DotProd{-}{=}\).
	\end{enumerate}
\end{beweis}
